1. Clone the repository in a local folder
2. In command prompt enter these instructions ( please be sure to have Docker in your local machine - https://www.docker.com/ ):
  ```sh
  docker-compose up -d
  docker-compose exec app sh ./lifecycle.sh
  ```
3. Open the following url in a web browser: "http://localhost:5000/www/public/index.html"
4. Enjoy! :) (PS: try with VCE AOT for example)
