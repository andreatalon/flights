<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$router = new AltoRouter();

// root
function matchRoot() { }
$router->map('GET', '/', function() { });

// healthcheck
$router->map('GET', '/healthcheck', function() { echo 'I\'m alive!'; return; });

// apis
$router->map('GET', '/api/[(airports|flights):api]', function($api) {
	require_once $_SERVER['DOCUMENT_ROOT'] . '/api/' . $api . '.php';
});

// others
$match = $router->match();
if(is_array($match) && is_callable($match['target'])) {
	return call_user_func_array($match['target'], $match['params']);
}

$full_php_path = \ltrim(\rtrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'), '/');
if (\file_exists($full_php_path)) {
	return require_once $_SERVER['DOCUMENT_ROOT'] . '/' . $full_php_path;
}

header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');

?>

