export const SET_AIRPORTS = 'SET.AIRPORTS';
export const SET_ARRIVAL = 'SET.ARRIVAL';
export const SET_DEPARTURE = 'SET.DEPARTURE';
export const SET_FLIGHTS = 'SET.FLIGHTS';
