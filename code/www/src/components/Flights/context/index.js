import { generateContext } from '../../../utils';
import reducer from './reducer';
import initialState from './state';
import actions from './actions';

const { FlightsProvider, useFlightsContext } = generateContext(
  'FlightsProvider',
  'useFlightsContext',
  initialState,
  reducer,
  actions
);

export { FlightsProvider, useFlightsContext };
