import React from 'react';
import { css, Global } from '@emotion/react';
import AppRouter from './routes';
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  return (
    <>
      <Global
        styles={css`
          body {
            color: var(--text-primary);
            background: var(--bg);
            transition-duration: 0.3s;
            transition-timing-function: ease;
            transition-property: border, background, color;
          }

          main {
            flex: 1;
            margin: 0 auto;
            max-width: 1280px;
            display: flex;
            flex-direction: column;
          }
        `}
      />
      <Header />
      <main><AppRouter /></main>
      <Footer />
    </>
  );
}

export default App;
