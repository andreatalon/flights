const URL = '/api/flights';

export default async ({ departure, arrival, successCB, failCB }) => {
  try {
    const response = await fetch(`${URL}?d=${departure}&a=${arrival}`);
    const data = await response.json();
    return (typeof successCB === 'function' && successCB(data)) || data;
  } catch (e) {
    return (typeof failCB === 'function' && failCB(e)) || e;
  }
};
