import { SET_AIRPORTS, SET_ARRIVAL, SET_DEPARTURE, SET_FLIGHTS } from './types';

export const setAirports = (payload) => ({ type: SET_AIRPORTS, payload });
export const setArrival = (payload) => ({ type: SET_ARRIVAL, payload });
export const setDeparture = (payload) => ({ type: SET_DEPARTURE, payload });
export const setFlights = (payload) => ({ type: SET_FLIGHTS, payload });
