import React, { useEffect } from 'react';
import { css } from '@emotion/react';
import Flights from '../components/Flights';
import { FlightsProvider } from '../components/Flights/context';

const HomePageStyle = css`
  h1 {
    font-size: 3rem;
    font-weight: 800;
    text-align: center;
  }
`;

const HomePage = () => {
  return (
    <div css={[HomePageStyle]}>
        <h1 className="title">Welcome!</h1>
        <br/>
        <FlightsProvider>
          <Flights/>
        </FlightsProvider>
    </div>
  );
};

export default HomePage;
