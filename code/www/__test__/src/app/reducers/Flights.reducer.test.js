'use strict'

import reducer from '../../../../src/components/Flights/context/reducer'
import { SET_DEPARTURE, SET_ARRIVAL } from '../../../../src/components/Flights/context/types'

describe('Flights reducer', () => {
  it('should be assign departure and arrival', () => {
    const initialState = {
      departure: '',
      arrival: '',
    }

    const newState = reducer(initialState, {
      type: SET_DEPARTURE,
      payload: 'VCE',
    })

    expect(newState.departure).toEqual('VCE')

    const newerState = reducer(newState, {
      type: SET_ARRIVAL,
      payload: 'AOT',
    })

    expect(newerState.arrival).toEqual('AOT')
  })
})
