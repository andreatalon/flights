<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

final class FlightsTest extends TestCase
{
	public function testFlightsCall(): void
    {
		$client = new Client(['base_uri' => 'http://flights-web-1']);
		$response = $client->request('GET', '/api/flights');

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];
		$this->assertEquals("application/json; charset=utf-8", $contentType);

		$json = json_decode($response->getBody()->getContents(), true);
		// fwrite(STDERR, print_r($json, true));
		// fwrite(STDERR, print_r(gettype($json), true));
		$this->assertIsArray($json);
		$this->assertArrayHasKey('flights', $json);
		$this->assertGreaterThanOrEqual(0, count($json['flights']));
	}
}

?>
