<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class MySQLTest extends TestCase
{
    public function testMySQLConnection(): void
    {
		$mysqli = new mysqli('mysql', 'root', '', 'homestead');
		$res = $mysqli->query("SELECT * FROM airport;");

		// if ($mysqli->affected_rows > 0) {
		// 	while ($row = $res->fetch_assoc()) {
		// 		$arr['airports'][] = $row;
		// 	}
		// }
        $this->assertIsObject($mysqli);
        $this->assertIsObject($res);
    }
}

?>
