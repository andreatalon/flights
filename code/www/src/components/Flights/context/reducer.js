import { SET_DEPARTURE, SET_ARRIVAL, SET_AIRPORTS, SET_FLIGHTS } from './types';

export default (state, action) => {
  switch (action.type) {
    case SET_AIRPORTS:
      return {
        ...state,
        airports: action.payload.airports?.sort(),
      };
    case SET_ARRIVAL:
      return {
        ...state,
        arrival: action.payload || '',
      };
    case SET_DEPARTURE:
      return {
        ...state,
        departure: action.payload || '',
      };
    case SET_FLIGHTS:
      return {
        ...state,
        flights: action.payload.flights?.sort(),
      };
    default:
      return {
        ...state,
      };
  }
};
