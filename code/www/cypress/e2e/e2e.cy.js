'use strict'

// import 'regenerator-runtime/runtime'
// import {cy} from 'cypress'
// import Xvfb from 'xvfb'

describe('e2e', () => {
  it('should check if app is reachable', () => {
    cy.visit('http://flights-web-1/www/public/index.html')

    cy.get('#departure').should('be.enabled')
    cy.get('#arrival').should('be.enabled')
  })

  it('should check if there are results for VCE -> AOT', () => {
    cy.visit('http://flights-web-1/www/public/index.html')

    cy.get('#departure').select('VCE')
    cy.get('#arrival').select('AOT')

    cy.wait(1200)

    cy.get('#results').should('not.be.empty')
  })
})
