import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { css } from '@emotion/react';
import { Media } from '../utils';

const headerStyle = css`
  height: 60px;

  ${Media.medium} {
    height: 50px;
  }

  & > nav {
    height: 100%;
    display: flex;
    padding: 0 12px;
    margin: 0 auto;
    max-width: 1280px;
    align-items: center;
    justify-content: space-between;

    .logo a {
      height: 100%;
      display: flex;
      align-items: center;
      font-size: 1.4rem;
      font-weight: bold;
      text-decoration: none;
    }

    svg {
      cursor: pointer;
      color: var(--primary);
      fill: var(--primary);
    }

    svg.theme {
      display: flex;
      user-select: none;
    }
  }
`;

const Header = () => {
  const { pathname } = useLocation();

  return (
    <header css={[headerStyle]}>
      <nav>
        <div className="logo">
          <Link to="/" replace={pathname === '/'}>Home/</Link>
        </div>
      </nav>
    </header>
  );
};

export default Header;
