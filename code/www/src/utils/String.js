export const hash = () => {
  let text = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  new Array(5).fill(undefined).map(() => {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  });
  return text;
};
