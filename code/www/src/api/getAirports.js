const URL = '/api/airports';

export default async ({ successCB, failCB }) => {
  try {
    const response = await fetch(`${URL}`);
    const data = await response.json();
    return (typeof successCB === 'function' && successCB(data)) || data;
  } catch (e) {
    return (typeof failCB === 'function' && failCB(e)) || e;
  }
};
