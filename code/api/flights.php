<?php

header('Content-Type: application/json; charset=utf-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$arr = ['flights' => []];

try {

	$mysqli = new mysqli('mysql', 'root', '', 'homestead');

	$res = $mysqli->query("SELECT * FROM flight;");
	$flights = [];
	$flights_d = [];
	$flights_a = [];
	$d = "";
	$a = "";

	if (isset($_GET['d'])) {
		$d = $_GET['d'];
	}

	if (isset($_GET['a'])) {
		$a = $_GET['a'];
	}

	if ($mysqli->affected_rows > 0) {
		while ($row = $res->fetch_assoc()) {
			if (
				(empty($d) && empty($a))
				|| (
					$d === $row['code_departure']
					&& empty($a)
				)
				|| (
					$a === $row['code_arrival']
					&& empty($d)
				)
				|| (
					!empty($d)
					&& !empty($a)
					&& $d === $row['code_departure']
					&& $a === $row['code_arrival']
				)
			) {
				$flights[] = $row;
			} else if (!empty($d) && !empty($a)) {
				if ($d === $row['code_departure']) {
					$flights_d[] = $row;
				} else if ($a === $row['code_arrival']) {
					$flights_a[] = $row;
				}
			}
		}
	}

	if (!empty($flights_d) && !empty($flights_d)) {
		usort($flights_d, function($x, $y) {
			return $x['price'] - $y['price'];
		});

		usort($flights_a, function($x, $y) {
			return $x['price'] - $y['price'];
		});

		foreach ($flights_d as $f_d) {
			foreach ($flights_a as $f_a) {
				if ($f_d['code_arrival'] == $f_a['code_departure']) {
					$flights[] = [
						'code_departure' => $d,
						'code_arrival' => $a,
						'stopover' => 1 .'('. $f_d['code_arrival'] .')',
						'price' => $f_d['price'] + $f_a['price']
					];
				}
			}
		}
	}

	$arr['flights'] = $flights;

} catch(Exception $e) {

	echo json_encode(['error' => 'Message: ' .$e->getMessage()]);
	die();

}

echo json_encode($arr);

?>
