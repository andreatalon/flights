#!/usr/bin/env bash

echo "Installing dependencies..."

if [ ! -d vendor ]; then
	composer install
fi

cd www

if [ ! -d node_modules ]; then
	npm i
fi

echo "Building..."

npm run build
if [ $? -ne 0 ]; then
   echo "BUILD FAILED"
   exit 1
fi

echo "Testing..."

cd ..

composer run-script test
if [ $? -ne 0 ]; then
   echo "TESTS FAILED"
   exit 1
fi

cd www

npm run test
if [ $? -ne 0 ]; then
   echo "TESTS FAILED"
fi

echo "Done! Enjoy it!"
