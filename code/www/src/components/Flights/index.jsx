import React, { useEffect } from 'react';
import { css } from '@emotion/react';
import { hash } from '../../utils';
import getAirports from '../../api/getAirports';
import getFlights from '../../api/getFlights';
import { useFlightsContext } from './context';

const FlightsStyle = css`
  fieldset {
    margin: 2rem 0;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  legend {
    text-tranform: uppercase;
    font-size: 1.5rem;
    font-weight: 600;
    margin: 0 auto;
  }

  select {
    padding: 1rem;
    margin: 1rem;
    min-width: 14rem;
  }

  button {
    font-size: 3rem;
    margin: 1rem;
    padding: 0 0.5rem;
  }

  form + div,
  form + div + div {
    padding: 0 2rem;
  }

  form + div + div {
    max-height: 400px;
    overflow-y: auto;
    margin-bottom: 2rem;
  }

  table {
    width: 100%;
  }

  th, td {
    padding: 1em;
  }

  th {
    font-size: 1.3rem;
    font-weight: 600;
  }

  td {
    text-align: center;
    border-bottom: 1px dashed #ededed;
    font-size: 1.2rem;
  }

  th:nth-of-type(2),
  td:nth-of-type(2) {
    font-size: 0.9rem;
    font-style: italic;
  }

  td:nth-of-type(4) {
    text-align: right;
  }
`;

const Flights = () => {
  const [
    { airports, flights, departure, arrival },
    { setAirports, setFlights, setDeparture, setArrival }
  ] = useFlightsContext()

  const changeDeparture = e => setDeparture(e.target.value)
  const changeArrival = e => setArrival(e.target.value)
  const reset = e => { setDeparture(''); setArrival(''); e.preventDefault(); }

  useEffect(() => {
    getAirports({
      successCB: data => {
        setAirports(data)
      },
    });
  }, [])

  useEffect(() => {
    getFlights({
      departure,
      arrival,
      successCB: data => {
        setFlights(data)
      },
    });
  }, [departure, arrival])

  return (
    <div css={[FlightsStyle]}>
      <form>
        <fieldset>
          <legend>Search for the best flight:</legend>
          <select id="departure" onChange={changeDeparture} value={departure}>
            <option value={''}>--</option>
            {airports.map(a => {
              const disabled = flights.filter(f => f.code_departure === a.code).length === 0
              return <option disabled={disabled} key={`d-${a.id}`} value={`${a.code}`}>{a.name}</option>
            })}
          </select>
          <select id="arrival" onChange={changeArrival} value={arrival}>
            <option value={''}>--</option>
            {airports.map(a => {
              const disabled = flights.filter(f => f.code_arrival === a.code).length === 0
              return <option disabled={disabled} key={`a-${a.id}`} value={`${a.code}`}>{a.name}</option>
            })}
          </select>
          <button onClick={reset}>&#8634;</button>
        </fieldset>
      </form>
      <div>
        <table>
          <thead>
            <tr>
              <th width="35%">Departure</th>
              <th width="10%">so</th>
              <th width="35%">Arrival</th>
              <th width="20%">Price</th>
            </tr>
          </thead>
        </table>
      </div>
      {flights.length > 0 && <div>
        <table>
          <tbody id="results">
            {flights.sort((a, b) => a.price - b.price).map(f => <tr key={`flight-${f.id}-${hash()}`}>
              <td width="35%">{f.code_departure}</td>
              <td width="10%">{f.stopover || 'dir'}</td>
              <td width="35%">{f.code_arrival}</td>
              <td width="20%">{f.price}&euro;</td>
            </tr>)}
          </tbody>
        </table>
      </div>}
    </div>
  );
};

export default Flights;
