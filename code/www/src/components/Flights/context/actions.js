import { setAirports, setArrival, setDeparture, setFlights } from './action-creators';

export default (dispatch) => ({
  setAirports: (payload) => dispatch(setAirports(payload)),
  setArrival: (payload) => dispatch(setArrival(payload)),
  setDeparture: (payload) => dispatch(setDeparture(payload)),
  setFlights: (payload) => dispatch(setFlights(payload)),
});
