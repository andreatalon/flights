<?php

header('Content-Type: application/json; charset=utf-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$arr = ['airports' => []];

try {

	$mysqli = new mysqli('mysql', 'root', '', 'homestead');

	$res = $mysqli->query("SELECT * FROM airport;");

	if ($mysqli->affected_rows > 0) {
		while ($row = $res->fetch_assoc()) {
			$arr['airports'][] = $row;
		}
	}

} catch(Exception $e) {

	echo json_encode(['error' => 'Message: ' .$e->getMessage()]);
	die();

}

echo json_encode($arr);

?>
