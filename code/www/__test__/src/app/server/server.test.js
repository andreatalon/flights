'use strict'

import 'regenerator-runtime/runtime'
import request from 'supertest'

const be = 'http://flights-web-1'
let res

describe('be', () => {
  test('/api/airports', async () => {
    res = await request(be)
      .get('/api/airports')
      .send()
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('airports')
    expect(res.body.airports).toBeDefined()
  })
})
